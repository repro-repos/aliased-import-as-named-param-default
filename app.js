import { thing as defaultThing, safe } from "./defaults.js";

export function stuffDoer( { notAThing = defaultThing, thing = defaultThing, safeStuff = safe } = {} ){
	if( thing ){
		thing();
	}

	if( safeStuff ){
		safeStuff();
	}

	if( defaultThing ){
		defaultThing();
	}
}